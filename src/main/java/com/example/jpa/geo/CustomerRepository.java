package com.example.jpa.geo;

import java.util.List;

import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	List<Customer> findByLastName(String lastName);

	@Query(value = "Select t from #{#entityName} t" + " where within(t.geom, :area) = true")
	List<Customer> findByGeomWithin(Geometry area);

	Customer findById(long id);
}