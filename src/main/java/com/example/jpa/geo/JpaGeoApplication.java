package com.example.jpa.geo;

import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.io.WKTReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JpaGeoApplication {

	private static final Logger log = LoggerFactory.getLogger(JpaGeoApplication.class);

	private static final WKTReader wkt = new WKTReader(new GeometryFactory(new PrecisionModel(), 4326));

	public static void main(String[] args) {
		SpringApplication.run(JpaGeoApplication.class);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
			
			//clear
			repository.deleteAll();

			// save a few customers
			repository.save(new Customer("Otto", "Bauer", (Point) wkt.read("POINT Z(-13.381419 52.509535 0)"))); // Berlin
			repository.save(new Customer("Chantal", "Clemont", (Point) wkt.read("POINT Z(2.395466 48.835797 0)"))); // Paris
			repository.save(new Customer("Dave", "Bauer", (Point) wkt.read("POINT Z(-74.034068 34.052659 0)"))); // NYC
			repository.save(new Customer("David", "Palmer", (Point) wkt.read("POINT Z(-118.258173 -9.31640625 0)"))); // LA
			repository.save(new Customer("Michelle", "Meghan", (Point) wkt.read("POINT Z(-208.789595 -33.85217 0)"))); // Sydney

			// fetch all customers in Europe
			log.info("Customers found within Europe:");
			log.info("-------------------------------");
			for (Customer customer : repository.findByGeomWithin((Polygon)wkt.read("POLYGON((-22 35, -22 58, 32 58, -22 35))"))) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			repository.findByLastName("Bauer").forEach(bauer -> {
				log.info(bauer.toString());
			});
			log.info("");
		};
	}

}