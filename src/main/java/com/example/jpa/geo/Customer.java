package com.example.jpa.geo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.locationtech.jts.geom.Point;


@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String firstName;
	private String lastName;
	private Point geom;

	protected Customer() {
	}

	public Customer(String firstName, String lastName, Point geom) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.geom = geom;
	}

	@Override
	public String toString() {
		return String.format("Customer[id=%d, firstName='%s', lastName='%s', srid='%d' geom='%s']", id, firstName, lastName,
				geom.getSRID(), geom);
	}

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Point getGeom() {
		return geom;
	}
}