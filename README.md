# sample-jpa-geo

This project includes a simple Spring Boot application that uses JPA to store geometric data types in 
geodatabases such as Postgis or H2Geo.

## Step 1: Store customers with a location to database

```java
// save a few customers with a point geometry
repository.save(new Customer("Otto", "Bauer", (Point) wkt.read("POINT(-13.381419 52.509535)"))); // Berlin
repository.save(new Customer("Chantal", "Clemont", (Point) wkt.read("POINT(2.395466 48.835797)"))); // Paris
repository.save(new Customer("Dave", "Bauer", (Point) wkt.read("POINT(-74.034068 34.052659)"))); // NYC
repository.save(new Customer("David", "Palmer", (Point) wkt.read("POINT(-118.258173 -9.31640625)"))); // LA
repository.save(new Customer("Michelle", "Meghan", (Point) wkt.read("POINT(-208.789595 -33.85217)"))); // Sydney
```

## Step 2: Retrieve customers by a spatial query (within a polygon)
Create a spatial query in a CRUD-Interface. Topological queries are delegated to database.

```java
//select customers by a spatial query (simple boundary box of Europe)
repository.findByGeomWithin((Polygon)wkt.read("POLYGON((-22 35, -22 58, 32 58, -22 35))"))
```

## Step 3: Check the results
Results:
```txt
com.example.jpa.geo.JpaGeoApplication    : Started JpaGeoApplication in 2.46 seconds (JVM running for 3.022)
com.example.jpa.geo.JpaGeoApplication    : Customers found within Europe:
com.example.jpa.geo.JpaGeoApplication    : -------------------------------
com.example.jpa.geo.JpaGeoApplication    : Customer[id=11, firstName='Otto', lastName='Bauer', srid='4326' geom='POINT (-13.381419 52.509535)']
com.example.jpa.geo.JpaGeoApplication    : Customer[id=12, firstName='Chantal', lastName='Clemont', srid='4326' geom='POINT (2.395466 48.835797)']
com.example.jpa.geo.JpaGeoApplication    : 
com.example.jpa.geo.JpaGeoApplication    : Customer found with findByLastName('Bauer'):
com.example.jpa.geo.JpaGeoApplication    : --------------------------------------------
com.example.jpa.geo.JpaGeoApplication    : Customer[id=11, firstName='Otto', lastName='Bauer', srid='4326' geom='POINT (-13.381419 52.509535)']
com.example.jpa.geo.JpaGeoApplication    : Customer[id=13, firstName='Dave', lastName='Bauer', srid='4326' geom='POINT (-74.034068 34.052659)']
com.example.jpa.geo.JpaGeoApplication    : 
```
